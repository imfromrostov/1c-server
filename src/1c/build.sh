#!/bin/sh

TAG=8.3.18-1208
docker image rm its.local/1c-server -f
sudo docker build --tag itsbz/1c-server:$TAG .
sudo docker push itsbz/1c-server
sudo docker push itsbz/1c-server:$TAG
