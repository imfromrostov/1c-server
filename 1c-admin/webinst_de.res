  �'      ResB             � P   �   �	  �	         �	  IDS_USAGE_WIN IDS_USAGE_LINUX IDS_UNKNOWN_PARAMETER IDS_IIS_NOTFOUND IDS_PATH_REQUIRED IDS_NAME_REQUIRED IDS_CONNSTR_REQUIRED IDS_CONFPATH_REQUIRED IDS_INCOMP_KEYS IDS_WEBSRV_REQUIRED IDS_WSDIR_OR_DESCRIPTOR_REQUIRED IDS_DIR_KEY_REQUIRED IDS_CONNSTR_OR_DESCRIPTOR_KEY_REQUIRED IDS_PUB_NOTFOUND IDS_WRONG_CONNECTIONSTRING IDS_WRONG_DIRECTORY IDS_WRONG_DIRECTORY1 IDS_DELETE_SUCCESS IDS_PUB_SUCCESS IDS_PUB_UPDATED IDS_OBSOLETTE_KEY IDS_EXCEPTION IDS_WWWROOT_DENIED IDS_WEBINST_NOADAPTER IDS_DESCRIPTOR_REQUIRED IDS_DESCRIPTORNOTFOUND IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_WINDOWS IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_LINUX �  A u s n a h m e :     w e b s r v      M u s s p a r a m e t e r   V e r � f f e n t l i c h u n g   a u s g e f � h r t   V e r � f f e n t l i c h u n g   d e a k t i v i e r t   V e r � f f e n t l i c h u n g   a k t u a l i s i e r t   D e s k r i p t o r   w u r d e   n i c h t   g e f u n d e n   V e r � f f e n t l i c h u n g   n i c h t   g e f u n d e n   W e b - S e r v e r   I I S   i s t   n i c h t   g e f u n d e n .   U n b e k a n n t e r   P a r a m e t e r   d e r   B e f e h l s z e i l e :     .�D i e   S c h l � s s e l   % s   u n d   % s   s i n d   n i c h t   k o m p a t i b e l .   /�D e r   S c h l � s s e l   % s   i s t   v e r a l t e t ,   v e r w e n d e n   S i e   % s   5�V e r b i n d u n g s z e i l e   s o l l   i m   P a r a m e t e r   a n g e g e b e n   w e r d e n :     6�D e r   K a t a l o g   % s   k a n n   k e i n   V e r � f f e n t l i c h u n g s k a t a l o g   s e i n   8�- d i r   -   e r f o r d e r l i c h e r   P a r a m e t e r   b e i   d e r   V e r � f f e n t l i c h u n g   <�D e r   S c h l � s s e l     - w s d i r   o d e r    d e s c r i p t o r   s o l l   a n g e g e b e n   w e r d e n   <�D e r   P f a d   z u r   . v r d - D a t e i   s o l l   i m   P a r a m e t e r   a n g e g e b e n   w e r d e n :     >�N a m e   d e r   V e r � f f e n t l i c h u n g   s o l l   i m   P a r a m e t e r   a n g e g e b e n   w e r d e n :     >�D e r   S c h l � s s e l     - c o n n s t r   o d e r    d e s c r i p t o r   s o l l   a n g e g e b e n   w e r d e n   ?�P f a d   z u r   c o n f - D a t e i   A p a c h e   s o l l   i m   P a r a m e t e r   a n g e g e b e n   w e r d e n :     E�V e r z e i c h n i s   d e r   V e r � f f e n t l i c h u n g   s o l l   i m   P a r a m e t e r   a n g e g e b e n   w e r d e n :     S�D a s   D e s k r i p t o r v e r z e i c h n i s   s t i m m t   m i t   d e m   V e r � f f e n t l i c h u n g s v e r z e i c h n i s   n i c h t   � b e r e i n   ]�V e r z e i c h n i s   d e s   P a r a m e t e r s   - d i r   s t i m m t   m i t   d e m   V e r z e i c h n i s   d e r   V e r � f f e n t l i c h u n g   n i c h t   � b e r e i n   k�V e r b i n d u n g s z e i l e   d e s   P a r a m e t e r s   - c o n n s t r   s t i m m t   m i t   d e r   V e r b i n d u n g s z e i l e   d e r   V e r � f f e n t l i c h u n g   n i c h t   � b e r e i n   ��D i e   E r w e i t e r u n g s m o d u l e   d e s   W e b - S e r v e r s   w u r d e n   n i c h t   g e f u n d e n . 
 Z u m   A u s f � h r e n   d e r   V e r � f f e n t l i c h u n g   m u s s   d i e   I n s t a l l a t i o n   v o n   1 C : E n t e r p r i s e   g e � n d e r t   w e r d e n .   ��U m   d i e s e   A k t i o n   a u s z u f � h r e n ,   s i n d   S u p e r u s e r - B e r e c h t i g u n g e n   ( r o o t )   e r f o r d e r l i c h . 
 A n s o n s t e n   l � u f t   d a s   P r o g r a m m   m � g l i c h e r w e i s e   n i c h t   o r d n u n g s g e m � � . 
 U m   d i e   A k t i o n   i m   S u p e r u s e r - M o d u s   z u   s t a r t e n ,   f � h r e n   S i e   d e n   B e f e h l   � b e r   s u d o   a u s .   �D i e s e   A k t i o n   d a r f   n u r   d e r   A d m i n i s t r a t o r   d e s   B e t r i e b s s y s t e m s   a u s f � h r e n . 
 A n s o n s t e n   l � u f t   d a s   P r o g r a m m   m � g l i c h e r w e i s e   n i c h t   o r d n u n g s g e m � � . 
 U m   d i e   A k t i o n   a l s   A d m i n i s t r a t o r   z u   s t a r t e n ,   s t a r t e n   S i e   d i e   B e f e h l s z e i l e   i m   A d m i n i s t r a t o r - M o d u s   u n d   f � h r e n   S i e   d e n   B e f e h l   i n   d i e s e r   B e f e h l s z e i l e   a u s .   ���1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   �ߣ1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - i i s :   ?C1;8:0F8O  51- :;85=B0  4;O  I I S  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   ( B>;L:>  4;O  ?C1;8:0F88  =0  A p a c h e )  
                 - o s a u t h :   8A?>;L7>20=85  W i n d o w s   02B>@870F88  ( B>;L:>  4;O  ?C1;8:0F88  =0  I I S )    c5� � �� �T � w �e 5��> .   �� Fau� �P4�g? z N� � �6M� # \ � S>� H�����